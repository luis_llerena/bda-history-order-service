﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BDA.DataAccess.Oracle
{
    public abstract class OracleRepository : Repository<OracleConnection>
    {
        protected IList<T> ReadList<T>(string sql, CommandType commandType, Func<OracleDataReader, T> read, params OracleParameter[] parameters)
        {
            return ReadEx(sql, commandType, read, null, parameters);
        }

        protected IList<T> ReadEx<T>(string sql, CommandType commandType, Func<OracleDataReader, T> readFromReader, Action<Dictionary<string, OracleParameter>> readFromOutParameters, params OracleParameter[] parameters)
        {
            return ReadExInternal(sql, commandType, readFromReader, readFromOutParameters, parameters);
        }

        protected void Execute(string sql, CommandType commandType, Action<Dictionary<string, OracleParameter>> readFromOutParameters, params OracleParameter[] parameters)
        {
            ReadExInternal<object>(sql, commandType, null, readFromOutParameters, parameters);
        }

        protected IList<T> ReadExInternal<T>(string sql, CommandType commandType, Func<OracleDataReader, T> readFromReader, Action<Dictionary<string, OracleParameter>> readFromOutParameters, params OracleParameter[] parameters)
        {
            //using (new OracleSessionSetup(Connection) { IsStringCompareCaseSensitive = false })
            using (var cmd = new OracleCommand(sql, Connection))
            {
                cmd.BindByName = true;
                cmd.CommandType = commandType;
                cmd.Parameters.AddRange(parameters);

                var result = new List<T>();

                using (var reader = cmd.ExecuteReader())
                {
                    if (readFromReader != null)
                    {
                        while (reader.Read())
                        {
                            result.Add(readFromReader(reader));
                        }
                    }
                }

                if (readFromOutParameters != null)
                {
                    var p = parameters.Where(par => par.Direction != ParameterDirection.Input).ToArray();
                    if (p.Length > 0)
                    {
                        readFromOutParameters(p.ToDictionary(par => par.ParameterName));
                    }
                }

                return result;
            }
        }

        #region Nested Types

        ///// <summary>
        ///// Defines various options to use inside a single connection.
        ///// </summary>
        //public class OracleSessionSetup : IDisposable
        //{
        //    #region Fields

        //    private readonly OracleConnection _c;
        //    private readonly OracleGlobalization _sessionInfo;

        //    #endregion Fields

        //    #region Construction

        //    public OracleSessionSetup(OracleConnection c)
        //    {
        //        _c = c;
        //        _sessionInfo = c.GetSessionInfo();

        //        _isStringCompareCaseSensitive = true;
        //    }

        //    #endregion Construction

        //    #region IDisposable Members

        //    public void Dispose()
        //    {
        //        _c.SetSessionInfo(_sessionInfo);
        //    }

        //    #endregion IDisposable Members

        //    #region IsStringCompareCaseSensitive

        //    private bool _isStringCompareCaseSensitive;

        //    public bool IsStringCompareCaseSensitive
        //    {
        //        get { return _isStringCompareCaseSensitive; }
        //        set
        //        {
        //            if (_isStringCompareCaseSensitive != value)
        //            {
        //                _isStringCompareCaseSensitive = value;

        //                var info = _c.GetSessionInfo();

        //                // Comparison: http://docs.oracle.com/cd/B19306_01/server.102/b14237/initparams120.htm#sthref374
        //                // Sort: http://docs.oracle.com/cd/B19306_01/server.102/b14237/initparams130.htm#REFRN10127
        //                if (!_isStringCompareCaseSensitive)
        //                {
        //                    info.Comparison = "LINGUISTIC";
        //                    info.Sort = "BINARY_CI";
        //                }
        //                else
        //                {
        //                    info.Comparison = "BINARY";
        //                    info.Sort = "BINARY";
        //                }

        //                _c.SetSessionInfo(info);
        //            }
        //        }
        //    }

        //    #endregion IsStringCompareCaseSensitive
        //}

        #endregion Nested Types
    }
}
