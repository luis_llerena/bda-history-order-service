﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;

namespace BDA.DataAccess.Oracle
{
    public class OracleQuery
    {
        private readonly IList<OracleParameter> _parameters = new List<OracleParameter>();
        private readonly IDictionary<string, string> _constants = new Dictionary<string, string>();

        protected string GenerateInputAssignments(string objectName, IList<OracleParameter> parameters)
        {
            var sb = new StringBuilder();

            foreach (var op in parameters)
            {
                sb.AppendFormat("{0}.{1} := :{1};", objectName, op.ParameterName);
            }

            return sb.ToString();
        }

        protected string GenerateOutputAssignments(string objectName, IList<OracleParameter> parameters)
        {
            var sb = new StringBuilder();

            foreach (var op in parameters)
            {
                sb.AppendFormat(":{1} := {0}.{1};", objectName, op.ParameterName);
            }

            return sb.ToString();
        }

        public OracleQuery AddParameter(string name, object value, OracleDbType type)
        {
            var par = new OracleParameter(name, type, value, ParameterDirection.Input);

            _parameters.Add(par);

            return this;
        }

        public OracleQuery AddParameter(string name, object value, OracleDbType type, ParameterDirection direction)
        {
            var par = new OracleParameter(name, type, value, direction);

            _parameters.Add(par);

            return this;
        }

        public OracleQuery AddObjectParameter<T>(string name, T obj, ParameterDirection direction)
        {
            var par = new OracleParameter(name, OracleDbType.Object, obj, direction);

            _parameters.Add(par);

            return this;
        }

        public OracleQuery AddParameter(string name, OracleDbType type, int size)
        {
            var par = new OracleParameter(name, type, size);
            par.Direction = ParameterDirection.Output;

            _parameters.Add(par);

            return this;
        }

        public OracleQuery AddParameter(string name, OracleDbType type)
        {
            var par = new OracleParameter(name, type);
            par.Direction = ParameterDirection.Output;

            _parameters.Add(par);

            return this;
        }

        public OracleQuery AddParameterDirectly(string parameter, string value)
        {
            _constants.Add(new KeyValuePair<string, string>(parameter, value));

            return this;
        }

        public IDictionary<string, OracleParameter> ExecuteProcedure(OracleConnection c, string procedureName)
        {
            List<OracleParameter> allParameters;

            string sql = GenerateSql(procedureName, out allParameters);

            using (var cmd = new OracleCommand(sql, c))
            {
                cmd.BindByName = true;

                cmd.Parameters.AddRange(allParameters.ToArray());

                cmd.ExecuteNonQuery();
            }

            return allParameters
                .Where(p => p.Direction == ParameterDirection.InputOutput || p.Direction == ParameterDirection.Output)
                .ToDictionary(p => p.ParameterName);
        }

        private string GenerateSql(string procedureName, out List<OracleParameter> allParameters)
        {
            var query = new StringBuilder();

            var objParamsMap = _parameters
                .Where(p => p.OracleDbType == OracleDbType.Object)
                .ToDictionary(p => p, p => GetParameters(p.ParameterName, p.Value, p.Direction));

            var objectParameters = _parameters.Where(p => p.OracleDbType == OracleDbType.Object).ToList();
            if (objectParameters.Count > 0)
            {
                query.Append("DECLARE ");

                foreach (var par in objectParameters)
                {
                    var oracleType = par.Value.GetType().GetCustomAttributes(typeof(DescriptionAttribute), false).Cast<DescriptionAttribute>().First().Description;

                    query.AppendFormat("{0} {1};", par.ParameterName, oracleType);
                }
            }

            query.Append("BEGIN ");

            foreach (var par in objectParameters.Where(p => p.Direction == ParameterDirection.Input || p.Direction == ParameterDirection.InputOutput))
            {
                var objParams = objParamsMap[par];

                query.Append(GenerateInputAssignments(par.ParameterName, objParams));
            }

            var parameters = string.Join(",", _parameters.Select(p => p.OracleDbType == OracleDbType.Object ? p.ParameterName + " => " + p.ParameterName : p.ParameterName + "=>:" + p.ParameterName));
            var constants = string.Join(",", _constants.Select(c => c.Key + "=>" + c.Value));
            query.AppendFormat("{0}({1});", procedureName, String.IsNullOrEmpty(constants) ? parameters : parameters + "," + constants);

            foreach (var par in objectParameters.Where(p => p.Direction == ParameterDirection.Output || p.Direction == ParameterDirection.InputOutput))
            {
                var objParams = objParamsMap[par];

                query.Append(GenerateOutputAssignments(par.ParameterName, objParams));
            }

            query.Append("END;");

            allParameters = new List<OracleParameter>();
            allParameters.AddRange(_parameters.Where(p => p.OracleDbType != OracleDbType.Object));
            foreach (var objParams in objParamsMap.Values)
            {
                allParameters.AddRange(objParams);
            }

            return query.ToString();
        }

        protected List<OracleParameter> GetParameters(string name, object entity, ParameterDirection direction)
        {
            var result = new List<OracleParameter>();

            foreach (var property in entity.GetType().GetProperties())
            {
                var ma = property.GetCustomAttributes(typeof(MappingAttribute), true).Cast<MappingAttribute>().FirstOrDefault();

                if (ma == null)
                    continue;

                var value = property.GetValue(entity, null);

                // skip optional values that are nulls.
                if (ma.IsNullable && value == null)
                continue;

                var op = new OracleParameter(ma.Name, ma.DbType, value, direction);

                if (ma.Size.HasValue)
                    op.Size = ma.Size.Value;

                op.IsNullable = ma.IsNullable;

                result.Add(op);
            }

            return result;
        }
    }
}
