﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Diagnostics;

namespace BDA.DataAccess.Oracle
{
    public abstract class OracleContext<TRepository> : DataContext<TRepository, OracleConnection>
    {
        protected override OracleConnection CreateConnection()
        {
            if (string.IsNullOrEmpty(ConnectionString))
                throw new InvalidOperationException("Connection string is not set");

            var connection = new OracleConnection(ConnectionString);

            connection.Open();

            Debug.Print("Created {0} {1} in context {2}", connection.GetType().Name, connection.GetHashCode(), GetHashCode());

            return connection;
        }
    }
}
