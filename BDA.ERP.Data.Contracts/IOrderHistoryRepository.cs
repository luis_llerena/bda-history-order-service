﻿using BDA.OrderService.Objects.Order;
using System;
using System.Collections.Generic;

namespace BDA.ERP.Data.Contracts
{
    public interface IOrderHistoryRepository : IErpRepository
    {
        RawOrder GetOrder(string orderNumber);
        RawOrder GetOrderByBdacOrderNumber(string orderNumber);
        IList<RawOrder> GetOrders(int? accountId, string email, string program, string status, DateTime? fromDate, DateTime? toDate, int startRow, int endRow, string salesperson, bool? isMp, string customerPo, string zipCode);
        int GetOrdersCount(int? accountId, string email, string program, string status, DateTime? fromDate, DateTime? toDate, string salesperson, bool? isMp, string customerPo, string zipCode);
        IList<RawOrderItem> GetOrderItems(string[] orderNumbers);
        IList<Invoice> GetInvoicesByOrderNumber(string orderNumber);
        IList<InvoiceInfo> GetInvoicesForBdac(string orderNumber);
    }
}
