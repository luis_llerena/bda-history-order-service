﻿namespace BDA.ERP.Data.Contracts
{
    public interface ISalesOrderRepository : IErpRepository
    {
        bool CheckSalesOrder(string number);
    }
}
