﻿using BDA.OrderService.Objects;
using System.Collections.Generic;

namespace BDA.ERP.Data.Contracts
{
    public interface ISkuHistoryRepository : IErpRepository
    {
        int GetSkuOrdersCount(string sku);

        IList<SkuOrder> GetSkuOrders(string sku, int startRecord, int endRecord);

        int GetSkuInventoryMovesCount(string sku);

        IList<SkuInventoryMove> GetSkuInventoryMoves(string sku, int startRecord, int endRecord);
    }
}
