using System;

namespace BDA.ERP.Data.Contracts.Exceptions
{
    /// <summary>
    /// Base Oracle exception.
    /// </summary>
    public class ErpException : Exception
    {
        public ErpException()
        {
        }

        public ErpException(string message)
            : base(message)
        {
        }

        public ErpException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class ErpConnectionException: ErpException
    {
        public ErpConnectionException()
        {
        }

        public ErpConnectionException(string message)
            : base(message)
        {
        }

        public ErpConnectionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class ErpAuthorizationException : ErpException
    {
        public ErpAuthorizationException()
        {
        }

        public ErpAuthorizationException(string message)
            : base(message)
        {
        }

        public ErpAuthorizationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}