namespace BDA.ERP.Data.Contracts.Exceptions
{
    /// <summary>
    /// Expected error, validation or missing data
    /// </summary>
    public class ExpectedErpException : ErpException
    {
        public ExpectedErpException()
        {
        }

        public ExpectedErpException(string message)
            : base(message)
        {
        }
    }
}