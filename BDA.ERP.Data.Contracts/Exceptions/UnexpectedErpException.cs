﻿namespace BDA.ERP.Data.Contracts.Exceptions
{
    /// <summary>
    /// Unexpected error, cannot be corrected by the calling program.
    /// </summary>
    public class UnexpectedErpException : ErpException
    {
        public UnexpectedErpException()
        {
        }

        public UnexpectedErpException(string message)
            : base(message)
        {
        }
    }
}
