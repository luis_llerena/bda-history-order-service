using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace BDA.DataAccess
{
    public class DataContextFactory<TContext, TRepository> : IDataContextFactory<TRepository>, IDisposable
        where TContext : IDataContext<TRepository>, new()
    {
        private readonly IDictionary<string, string> _connectionStrings;
        private readonly ConcurrentDictionary<string, TContext> _contexts = new ConcurrentDictionary<string, TContext>();

        public DataContextFactory(params ContextConfiguration[] configurations)
        {
            _connectionStrings = configurations.ToDictionary(c => c.Name ?? string.Empty, c => c.ConnectionString);
        }

        public IDataContext<TRepository> GetContext(string name = null)
        {
            if (name == null) name = string.Empty;
            var cs = _connectionStrings[name];

            var c = _contexts.GetOrAdd(name, _ => new TContext {ConnectionString = cs});
            return c;
        }

        public void Dispose()
        {
            foreach (var c in _contexts.Values)
            {
                c.Dispose();
            }
        }
    }
}