namespace BDA.DataAccess
{
    public struct ContextConfiguration
    {
        public readonly string Name;
        public readonly string ConnectionString;

        public ContextConfiguration(string name, string connectionString)
        {
            Name = name;
            ConnectionString = connectionString;
        }

        public ContextConfiguration(string connectionString)
            : this(null, connectionString)
        {
        }
    }
}