﻿using System;

namespace BDA.DataAccess
{
    public abstract class Repository<TConnection>
        where TConnection : class, IDisposable
    {
        public DataContext<TConnection> Context { get; set; }

        protected TConnection Connection
        {
            get { return Context.Connection; }
        }
    }
}
