using System;

namespace BDA.DataAccess
{
    public interface IDataContext<TRepository> : IDisposable
    {
        string ConnectionString { get; set; }
        T GetRepository<T>() where T : TRepository;
        TRepository GetRepository(Type type);
    }
}