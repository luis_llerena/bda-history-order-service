namespace BDA.DataAccess
{
    public interface IDataContextFactory<TRepository>
    {
        IDataContext<TRepository> GetContext(string name = null);
    }
}