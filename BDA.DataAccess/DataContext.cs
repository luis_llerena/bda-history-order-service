using System;
using System.Collections.Generic;

namespace BDA.DataAccess
{
    public abstract class DataContext<TConnection> : IDisposable
        where TConnection : class, IDisposable
    {
        #region private fields

        private string _connectionString;
        private TConnection _connection;

        #endregion private fields

        #region properties

        public string ConnectionString
        {
            get { return _connectionString; }
            set
            {
                if (_connectionString != value)
                {
                    DestroyConnection();

                    _connectionString = value;
                }
            }
        }
        
        public TConnection Connection
        {
            get { return _connection ?? (_connection = CreateConnection()); }
        }

        #endregion properties

        #region protected 

        protected abstract TConnection CreateConnection();

        #endregion protected

        #region private helers

        private void DestroyConnection()
        {
            if (_connection == null) return;

            _connection.Dispose();
            _connection = null;
        }

        #endregion private helers

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                DestroyConnection();
            }
        }

        #endregion IDisposable
    }

    public abstract class DataContext<TRepository, TConnection> : DataContext<TConnection>, IDataContext<TRepository>, IDisposable
        where TConnection : class, IDisposable
    {
        #region private fields

        private readonly Dictionary<Type, object> _repos = new Dictionary<Type, object>();

        #endregion private fields

        #region constructors

        protected DataContext()
        {
            Initialize();
        }

        #endregion constructors
        
        #region IDataContext<TRepository>

        public bool IsDefault { get; set; }

        public T GetRepository<T>() where T : TRepository 
        {
            return (T) _repos[typeof (T)];
        }

        public TRepository GetRepository(Type type)
        {
            return (TRepository) _repos[type];
        }

        #endregion IDataContext<TRepository>

        #region protected

        protected abstract void RegisterRepositories();

        protected void RegisterRepository<TContract, TImplementation>()
            where TContract: TRepository
            where TImplementation : Repository<TConnection>, TContract, new()
        {
            _repos[typeof (TContract)] = new TImplementation {Context = this};
        }

        #endregion protected

        #region private helpers

        private void Initialize()
        {
            RegisterRepositories();
        }

        #endregion private helpers

    }
}