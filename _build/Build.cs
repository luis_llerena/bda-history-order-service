﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Nuke.Common;
using Nuke.Common.CI;
using Nuke.Common.Execution;
using Nuke.Common.Git;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.Coverlet;
using Nuke.Common.Tools.Docker;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.GitVersion;
using Nuke.Common.Tools.ReSharper;
using Nuke.Common.Utilities.Collections;
using Serilog;
using static Nuke.Common.EnvironmentInfo;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;
using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tools.Docker.DockerTasks;
using static Nuke.Common.Tools.ReSharper.ReSharperTasks;

[CheckBuildProjectConfigurations(TimeoutInMilliseconds = 2000)]
[ShutdownDotNetAfterServerBuild]
class Build : NukeBuild
{
    /// Support plugins are available for:
    ///   - JetBrains ReSharper        https://nuke.build/resharper
    ///   - JetBrains Rider            https://nuke.build/rider
    ///   - Microsoft VisualStudio     https://nuke.build/visualstudio
    ///   - Microsoft VSCode           https://nuke.build/vscode

    public static int Main() => Execute<Build>(x => x.Test);

    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    readonly Configuration Configuration = IsLocalBuild ? Configuration.Debug : Configuration.Release;

    [Solution] readonly Solution Solution;
    [GitRepository] readonly GitRepository GitRepository;
    [GitVersion(NoFetch = true)] readonly GitVersion GitVersion;

    AbsolutePath SourceDirectory => RootDirectory / "src";
    AbsolutePath TestsDirectory => RootDirectory / "tests";
    static AbsolutePath ArtifactsDirectory => RootDirectory / "artifacts";

    static AbsolutePath BuildArtifactsDirectory => ArtifactsDirectory / "build";

    static AbsolutePath DockerArtifactsDirectory => ArtifactsDirectory / "docker";

    [Parameter("Test report path")] readonly AbsolutePath TestReportsPath = ArtifactsDirectory / "test";

    [Parameter("Analysis report path")] readonly AbsolutePath AnalysisReportsPath = ArtifactsDirectory / "analysis";

    List<string> PublishAppNames = new();

    Target Clean => _ => _
        .Before(Restore)
        .Executes(() =>
        {
            SourceDirectory.GlobDirectories("**/bin", "**/obj", "**/publish").ForEach(DeleteDirectory);
            TestsDirectory.GlobDirectories("**/bin", "**/obj", "**/publish").ForEach(DeleteDirectory);
            TemporaryDirectory.GlobDirectories("**/ReSharper*").ForEach(DeleteDirectory);
            EnsureCleanDirectory(ArtifactsDirectory);
        });

    Target Restore => _ => _
        .Executes(() =>
        {
            DotNetRestore(s => s
                .SetProjectFile(Solution));
        });

    Target Publish => _ => _
        .DependsOn(Restore, Clean)
        .Executes(() =>
        {
            DotNetPublish(s => s
                .SetProject(Solution)
                .SetConfiguration(Configuration)
                .SetAssemblyVersion(GitVersion.AssemblySemVer)
                .SetFileVersion(GitVersion.AssemblySemFileVer)
                .SetInformationalVersion(GitVersion.InformationalVersion)
                .SetProperty("PublishDir", "publish")
                .EnableNoRestore());


            EnsureExistingDirectory(BuildArtifactsDirectory);

            SourceDirectory.GlobDirectories("**/ImportServices/*ImportService/publish")
                .ForEach(e =>
                {
                    var appName = e.Parent?.Name;
                    PublishAppNames.Add(appName);
                    MoveDirectory(e, BuildArtifactsDirectory / appName);
                });
        });

    Target BuildWithCodeAnalysis => _ => _
        .DependsOn(Clean)
        .Executes(() =>
        {
            ReSharperLogger = (type, output) =>
            {
                if (output.StartsWith("Warning: AddToWatches") || output.StartsWith("   at "))
                {
                    // This is bitbucket specific warning that piles the logs
                    // skip for clearance
                    return;
                }

                ProcessTasks.DefaultLogger(type, output);
            };

            ReSharperInspectCode(s => s
                .SetTargetPath(Solution)
                .AddPlugin("ReSharperPlugin.CognitiveComplexity", ReSharperPluginLatest)
                .AddPlugin("ReSharperPlugin.HeapView", ReSharperPluginLatest)
                .SetOutput(AnalysisReportsPath / "ReSharperAnalysis.xml")
                .SetProcessWorkingDirectory(Solution.Directory)
                .SetProcessArgumentConfigurator(c => c.Add("--build")));
        });

    Target Test => _ => _
        .DependsOn(BuildWithCodeAnalysis)
        .Executes(() =>
        {
            DotNetTest(s => s
                .SetProjectFile(Solution)
                // Solution will already be build in Debug config by analysis task
                .SetConfiguration(Configuration.Debug)
                .EnableNoRestore()
                .EnableNoBuild()
                .EnableCollectCoverage()
                .SetProperty("ExcludeByAttribute", "\\\"Obsolete,GeneratedCodeAttribute,CompilerGeneratedAttribute\\\"")
                .SetCoverletOutputFormat("cobertura")
                .SetCoverletOutput(TestReportsPath + "/")
                .SetLoggers("trx")
                .SetResultsDirectory(TestReportsPath));
        });

    Target BuildDockerImages => _ => _
        .DependsOn(Publish)
        .Executes(() =>
        {
            DockerBuild(s => s
                .SetFile(RootDirectory / "Dockerfile")
                .CombineWith(PublishAppNames, (ss, name) =>
                    ss
                        .SetPath(BuildArtifactsDirectory / name)
                        .SetBuildArg($"APPNAME={name}")
                        .SetTag(name.ToLower(), GitVersion.MajorMinorPatch)));
        });

    Target SaveDockerImages => _ => _
        .DependsOn(BuildDockerImages)
        .Executes(() =>
        {
            EnsureExistingDirectory(DockerArtifactsDirectory);
            DockerSave(s => s
                .CombineWith(PublishAppNames, (ss, name) =>
                    ss
                        .SetImages(name.ToLower())
                        .SetOutput(DockerArtifactsDirectory / $"{name}.tar")));
        });
}
