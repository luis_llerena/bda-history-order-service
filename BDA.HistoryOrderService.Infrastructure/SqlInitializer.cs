﻿using BDA.Commerce.WebServices.Data;
using BDA.Commerce.WebServices.Data.Contracts;
using BDA.DataAccess;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDA.OrderService.Infrastructure
{
    public class SqlInitializer
    {
        private const string DefaultConnectionStringKey = "bdaCommerceConnection";
        public static void Initialize(IServiceCollection services, params ContextConfiguration[] configurations)
        {
            if (configurations.Length == 0)
                configurations = new[]
                {
                    new ContextConfiguration(ConfigurationManager.ConnectionStrings[DefaultConnectionStringKey]
                        .ConnectionString)
                };

            // data context factory
            services.AddScoped<IDataContextFactory<IBdaCommerceRepository>>(ifact =>
                new DataContextFactory<BdaCommerceContext, IBdaCommerceRepository>(configurations));

            // default context (unnamed) / transient, as it is mantained by the factory
            services.AddTransient(ifact => ifact.GetService<IDataContextFactory<IBdaCommerceRepository>>().GetContext());

            // services

        }
    }
}
