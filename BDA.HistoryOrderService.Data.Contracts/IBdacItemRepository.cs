﻿using BDA.OrderService.Objects.Order;
using System.Collections.Generic;

namespace BDA.Commerce.WebServices.Data.Contracts
{
    public interface IBdacItemRepository : IBdaCommerceRepository
    {
        List<BdacItemVariant> GetItemVariants(IEnumerable<string> skuList);
    }
}