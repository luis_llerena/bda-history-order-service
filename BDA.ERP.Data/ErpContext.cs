﻿using System;
using BDA.DataAccess.Oracle;
using BDA.ERP.Data.Contracts;
using BDA.ERP.Data.Contracts.Exceptions;
using Oracle.ManagedDataAccess.Client;

namespace BDA.ERP.Data
{
    public class ErpContext : OracleContext<IErpRepository>
    {
        protected override OracleConnection CreateConnection()
        {
            try
            {
                return base.CreateConnection();
            }
            catch (Exception ex)
            {
                throw HandleException(ex);
            }
        }

        protected override void RegisterRepositories()
        {
            RegisterRepository<IGeographyRepository, GeographyRepository>();
            RegisterRepository<ISalesOrderRepository, SalesOrderRepository>();
            RegisterRepository<IOrderHistoryRepository, OrderHistoryRepository>();
            RegisterRepository<ISkuHistoryRepository, SkuHistoryRepository>();
        }

        private Exception HandleException(Exception ex)
        {
            var oracleException = TryFindException<OracleException>(ex);

            if (oracleException == null)
                return new Exception("Unknown exception", ex);

            switch (oracleException.Number)
            {
                case 1017: // invalid username/password
                    return new ErpAuthorizationException("Error authorizing on ERP server", oracleException);
                case 12154: // server name not resolved
                case 12541: // no listener on specified port
                case 12514: // wrong service
                    return new ErpConnectionException("Error connecting to ERP server", oracleException);
                default:
                    return new ErpException("ERP returned unexpected error during connection attempt", oracleException);
            }
        }

        private static TException TryFindException<TException>(Exception ex) where TException : Exception
        {
            while (ex != null)
            {
                var tex = ex as TException;
                if (tex != null)
                {
                    return tex;
                }
                ex = ex.InnerException;
            }
            return null;
        }
    }
}
