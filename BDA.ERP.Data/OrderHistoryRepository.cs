﻿using BDA.ERP.Data.Contracts;
using BDA.OrderService.Objects.Order;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;

namespace BDA.ERP.Data
{
    public class OrderHistoryRepository : ErpRepository, IOrderHistoryRepository
    {
        private const string GetOrderProcedureName = "BDAWEB.XXBDA_EBS_BDAC_ORDER_HISTORY.Get_Order";
        private const string GetOrderByBdacOrderNumberProcedureName = "BDAWEB.XXBDA_EBS_BDAC_ORDER_HISTORY.Get_Order_Details_BDAC_SO";
        private const string GetOrdersProcedureName = "BDAWEB.XXBDA_EBS_BDAC_ORDER_HISTORY.Get_Orders";
        private const string GetOrdersCountProcedureName = "BDAWEB.XXBDA_EBS_BDAC_ORDER_HISTORY.Get_Orders_Count";
        private const string GetOrderItemsProcedureName = "BDAWEB.XXBDA_EBS_BDAC_ORDER_HISTORY.Get_Orders_Items";
        private const string GetInvoicesByOrdernumber = "BDAWEB.OI_ORDER.GET_INVOICES_BY_ORDERNUMBER";
        private const string GetInvoicesForBdacProcedureName = "BDAWEB.XXBDA_EBS_BDAC_ORDER_HISTORY.Get_Invoices_For_BDAC_SO";

        private RawOrder ReadRawOrder(OracleDataReader reader)
        {
            return new RawOrder
            {
                OrderNumber = reader.GetDecimal(0).ToString(CultureInfo.InvariantCulture),
                StatusDescription = reader.IsDBNull(1) ? String.Empty : reader.GetString(1),
                OrderType = reader.IsDBNull(2) ? String.Empty : reader.GetString(2),
                OrderDate = reader.GetDateTime(3),
                AccountId = reader.IsDBNull(4) ? 0 : Convert.ToInt32(reader.GetDecimal(4)),
                PartyId = reader.IsDBNull(5) ? 0 : Convert.ToInt32(reader.GetDecimal(5)),
                AccountName = reader.IsDBNull(6) ? String.Empty : reader.GetString(6),
                ProgramCode = reader.IsDBNull(7) ? String.Empty : reader.GetString(7),
                ShopperEmail = reader.IsDBNull(8) ? String.Empty : reader.GetString(8),
                BillTo = new Address
                {
                    Name = reader.IsDBNull(9) ? String.Empty : reader.GetString(9),
                    Street = reader.IsDBNull(10) ? String.Empty : reader.GetString(10),
                    City = reader.IsDBNull(11) ? String.Empty : reader.GetString(11),
                    State = reader.IsDBNull(12) ? String.Empty : reader.GetString(12),
                    Country2DigitCode = reader.IsDBNull(13) ? String.Empty : reader.GetString(13),
                    PostalCode = reader.IsDBNull(14) ? String.Empty : reader.GetString(14),
                },
                ShipTo = new Address
                {
                    Name = reader.IsDBNull(15) ? String.Empty : reader.GetString(15),
                    Street = reader.IsDBNull(16) ? String.Empty : reader.GetString(16),
                    City = reader.IsDBNull(17) ? String.Empty : reader.GetString(17),
                    State = reader.IsDBNull(18) ? String.Empty : reader.GetString(18),
                    Country2DigitCode = reader.IsDBNull(19) ? String.Empty : reader.GetString(19),
                    PostalCode = reader.IsDBNull(20) ? String.Empty : reader.GetString(20),
                },
                ShipMethod = reader.IsDBNull(21) ? String.Empty : reader.GetString(21),
                Carrier = reader.IsDBNull(22) ? String.Empty : reader.GetString(22),
                ShippingTotal = reader.IsDBNull(23) ? 0 : reader.GetDecimal(23),
                RushTotal = reader.IsDBNull(24) ? 0 : reader.GetDecimal(24),
                TaxTotal = reader.IsDBNull(25) ? 0 : reader.GetDecimal(25),
                Discount = reader.IsDBNull(26) ? 0 : reader.GetDecimal(26),
                BillingProfile = reader.IsDBNull(27) ? String.Empty : reader.GetString(27),
                Currency = reader.IsDBNull(28) ? String.Empty : reader.GetString(28),
            };
        }

        public RawOrder GetOrder(string orderNumber)
        {
            return ReadList(GetOrderProcedureName,
                CommandType.StoredProcedure,
                ReadRawOrder,
                new OracleParameter("p_order_number", string.IsNullOrEmpty(orderNumber) ? null : (object)decimal.Parse(orderNumber)),
                new OracleParameter("x_recordset", OracleDbType.RefCursor, ParameterDirection.Output))
                .FirstOrDefault();
        }

        public RawOrder GetOrderByBdacOrderNumber(string orderNumber)
        {
            return ReadList(GetOrderByBdacOrderNumberProcedureName,
                CommandType.StoredProcedure,
                ReadRawOrder,
                new OracleParameter("p_bdac_number", orderNumber),
                new OracleParameter("x_recordset", OracleDbType.RefCursor, ParameterDirection.Output))
                .FirstOrDefault();
        }

        public IList<RawOrder> GetOrders(int? accountId, string email, string program, string status, DateTime? fromDate, DateTime? toDate, int startRow, int endRow, string salesperson, bool? isMp, string customerPo, string zipCode)
        {
            return ReadList(GetOrdersProcedureName,
                CommandType.StoredProcedure,
                ReadRawOrder,
                new OracleParameter("p_account_id", accountId),
                new OracleParameter("p_email_address", email),
                new OracleParameter("p_order_status", status),
                new OracleParameter("p_program_code", program),
                new OracleParameter("p_from_date", fromDate),
                new OracleParameter("p_to_date", toDate),
                new OracleParameter("p_start_record", startRow),
                new OracleParameter("p_end_record", endRow),
                new OracleParameter("p_mp_order_only", isMp.HasValue ? isMp.Value ? "Y" : "N" : null),
                new OracleParameter("p_salesrep_email", salesperson),
                new OracleParameter("p_customer_po", customerPo),
                new OracleParameter("p_zip_code", zipCode),
                new OracleParameter("x_recordset", OracleDbType.RefCursor, ParameterDirection.Output));
        }

        public int GetOrdersCount(int? accountId, string email, string program, string status, DateTime? fromDate, DateTime? toDate, string salesperson, bool? isMp, string customerPo, string zipCode)
        {
            var result = 0;
            Execute(GetOrdersCountProcedureName, CommandType.StoredProcedure,
                p =>
                {
                    result = ((OracleDecimal) p["x_count"].Value).ToInt32();
                },
                new OracleParameter("p_account_id", accountId),
                new OracleParameter("p_email_address", email),
                new OracleParameter("p_order_status", status),
                new OracleParameter("p_program_code", program),
                new OracleParameter("p_from_date", fromDate),
                new OracleParameter("p_to_date", toDate),
                new OracleParameter("p_mp_order_only", isMp.HasValue ? isMp.Value ? "Y" : "N" : null),
                new OracleParameter("p_salesrep_email", salesperson),
                new OracleParameter("p_customer_po", customerPo),
                new OracleParameter("p_zip_code", zipCode),
                new OracleParameter("x_count", OracleDbType.Decimal, ParameterDirection.ReturnValue));
            return result;
        }

        public IList<RawOrderItem> GetOrderItems(string[] orderNumbers)
        {
            var list = string.Join(",", orderNumbers);
            return ReadList(GetOrderItemsProcedureName,
              CommandType.StoredProcedure,
              reader =>
              {
                  var line = new RawOrderItem
                  {
                      OrderNumber = reader.GetDecimal(0).ToString(CultureInfo.InvariantCulture),
                      Sku = reader.IsDBNull(1) ? String.Empty : reader.GetString(1),
                      Description = reader.IsDBNull(2) ? String.Empty : reader.GetString(2),
                      LineOrderQuantity = reader.IsDBNull(3) ? 0 : Convert.ToInt32(reader.GetDecimal(3)),
                      LineShipQuantity = reader.IsDBNull(4) ? 0 : Convert.ToInt32(reader.GetDecimal(4)),
                      UnitPrice = reader.IsDBNull(5) ? 0 : reader.GetDecimal(5),
                      ShipDate = reader.IsDBNull(6) ? null : (DateTime?)reader.GetDateTime(6),
                      ShippingMethod = reader.IsDBNull(7) ? String.Empty : reader.GetString(7),
                      Carrier = reader.IsDBNull(8) ? String.Empty : reader.GetString(8),
                      ScacCode = reader.IsDBNull(9) ? String.Empty : reader.GetString(9),
                      FreightCode = reader.IsDBNull(10) ? String.Empty : reader.GetString(10),
                      DeliveryName = reader.IsDBNull(11) ? String.Empty : reader.GetString(11),
                      // 12 delivery detail id ignored
                      Status = reader.IsDBNull(13) ? String.Empty : reader.GetString(13),
                      // 14 delivery status ignored
                      TrackingNumber = reader.IsDBNull(15) ? String.Empty : reader.GetString(15),
                      UomConversionFactor = reader.IsDBNull(18) ? 1 : Convert.ToInt32(reader.GetDecimal(18)),
                      VendorCustomizationUrl = reader.IsDBNull(19) ? String.Empty : reader.GetString(19)

                  };
                  try
                  {
                      // HACK:  Needs to be fixed in EBS when Biplob returns
                      line.DeliveryRequestedQuantity = reader.IsDBNull(16) ? (int?)null : Convert.ToInt32(reader.GetDecimal(16));
                      line.DeliveryShippedQuantity = reader.IsDBNull(17) ? (int?)null : Convert.ToInt32(reader.GetDecimal(17));
                  }
                  catch { };
                  return line;
              },

            new OracleParameter("p_list_of_orders", list),
            new OracleParameter("x_recordset", OracleDbType.RefCursor, ParameterDirection.Output));
        }

        // todo: move grouping logic to the service
        [Obsolete]
        public IList<Invoice> GetInvoicesByOrderNumber(string orderNumber)
        {
            var invoiceDetailsList = ReadList(GetInvoicesByOrdernumber,
                                                   CommandType.StoredProcedure,
                                                   reader => new
                                                   {
                                                       INVOICE_NUMBER = reader.IsDBNull(1) ? String.Empty : reader.GetString(1),
                                                       LINE_NUMBER = reader.IsDBNull(2) ? 0 : Convert.ToInt32(reader.GetDecimal(2)),
                                                       LINE_TYPE = reader.IsDBNull(3) ? String.Empty : reader.GetString(3),

                                                       BILL_TO_NAME = reader.IsDBNull(4) ? String.Empty : reader.GetString(4),
                                                       BILL_TO_ADDRESS = reader.IsDBNull(5) ? String.Empty : reader.GetString(5),
                                                       BILL_TO_CITY = reader.IsDBNull(6) ? String.Empty : reader.GetString(6),
                                                       BILL_TO_STATE = reader.IsDBNull(7) ? String.Empty : reader.GetString(7),
                                                       BILL_TO_COUNTRY = reader.IsDBNull(8) ? String.Empty : reader.GetString(8),
                                                       BILL_TO_ZIP = reader.IsDBNull(9) ? String.Empty : reader.GetString(9),

                                                       SHIP_TO_NAME = reader.IsDBNull(10) ? String.Empty : reader.GetString(10),
                                                       SHIP_TO_ADDRESS = reader.IsDBNull(11) ? String.Empty : reader.GetString(11),
                                                       SHIP_TO_CITY = reader.IsDBNull(12) ? String.Empty : reader.GetString(12),
                                                       SHIP_TO_STATE = reader.IsDBNull(13) ? String.Empty : reader.GetString(13),
                                                       SHIP_TO_COUNTRY = reader.IsDBNull(14) ? String.Empty : reader.GetString(14),
                                                       SHIP_TO_ZIP = reader.IsDBNull(15) ? String.Empty : reader.GetString(15),

                                                       INVOICE_DATE = reader.IsDBNull(16) ? (DateTime?)null : reader.GetDateTime(16),
                                                       PAYMENT_TERM = reader.IsDBNull(17) ? String.Empty : reader.GetString(17),
                                                       SHIP_DATE = reader.IsDBNull(18) ? (DateTime?)null : reader.GetDateTime(18),
                                                       DUE_DATE = reader.IsDBNull(19) ? (DateTime?)null : reader.GetDateTime(19),
                                                       CUST_PO_NUMBER = reader.IsDBNull(20) ? String.Empty : reader.GetString(20),
                                                       ORDER_QUANTITY = reader.IsDBNull(21) ? 0 : Convert.ToInt32(reader.GetDecimal(21)),
                                                       BACKORDER_QTY = reader.IsDBNull(22) ? 0 : Convert.ToInt32(reader.GetDecimal(22)),
                                                       SHIP_QTY = reader.IsDBNull(23) ? 0 : Convert.ToInt32(reader.GetDecimal(23)),
                                                       UNIT_SELLING_PRICE = reader.IsDBNull(24) ? 0 : reader.GetDecimal(24),
                                                       ITEM_DESCRIPTION = reader.IsDBNull(25) ? String.Empty : reader.GetString(25),
                                                       FREIGHT = reader.IsDBNull(26) ? 0 : reader.GetDecimal(26),
                                                       TAX = reader.IsDBNull(27) ? 0 : reader.GetDecimal(27),
                                                       CHARGES = reader.IsDBNull(28) ? 0 : reader.GetDecimal(28),
                                                       DISCOUNT = reader.IsDBNull(29) ? 0 : reader.GetDecimal(29),
                                                       INVOICE_TOTAL = reader.IsDBNull(30) ? 0 : reader.GetDecimal(30),
                                                       PAYMENTS_APPLIED = reader.IsDBNull(31) ? 0 : reader.GetDecimal(31),
                                                       CARRIER_CODE = reader.IsDBNull(32) ? String.Empty : reader.GetString(32)
                                                   },
                                                   new OracleParameter("p_order_number", orderNumber),
                                                   new OracleParameter("x_recordset", OracleDbType.RefCursor, ParameterDirection.Output));

            var result = new List<Invoice>();

            if (invoiceDetailsList == null || !invoiceDetailsList.Any())
            {
                return result;
            }

            foreach (var invGroup in invoiceDetailsList.GroupBy(inv => inv.INVOICE_NUMBER))
            {
                var inv = invGroup.First();
                var invoice = new Invoice
                {
                    OrderNumber = orderNumber,
                    InvoiceNumber = inv.INVOICE_NUMBER,
                    BillTo = new Address
                    {
                        Name = inv.BILL_TO_NAME,
                        Street = inv.BILL_TO_ADDRESS,
                        City = inv.BILL_TO_CITY,
                        Country2DigitCode = inv.BILL_TO_COUNTRY,
                        PostalCode = inv.BILL_TO_ZIP,
                        State = inv.BILL_TO_STATE
                    },
                    ShipTo = new Address
                    {
                        Name = inv.SHIP_TO_NAME,
                        Street = inv.SHIP_TO_ADDRESS,
                        City = inv.SHIP_TO_CITY,
                        Country2DigitCode = inv.SHIP_TO_COUNTRY,
                        PostalCode = inv.SHIP_TO_ZIP,
                        State = inv.SHIP_TO_STATE
                    },
                    CarrierName = inv.CARRIER_CODE,
                    InvoiceDate = inv.INVOICE_DATE,
                    Terms = inv.PAYMENT_TERM,
                    ShipDate = inv.SHIP_DATE,
                    DueDate = inv.DUE_DATE,
                    PONumber = inv.CUST_PO_NUMBER,
                    TaxTotal = inv.TAX,
                    FreightTotal = inv.FREIGHT,
                    SpecialCharge = inv.CHARGES,
                    OrderDiscount = inv.DISCOUNT,
                    BalanceDue = (inv.INVOICE_TOTAL- inv.PAYMENTS_APPLIED),
                    InvoiceTotal = inv.INVOICE_TOTAL,
                    InvoiceDetailLines = new List<InvoiceLine>()
                };

                foreach (var invElement in invGroup.Where(x => x.LINE_TYPE == "0").OrderBy(y => y.LINE_NUMBER))
                {
                    invoice.InvoiceDetailLines.Add(new InvoiceLine
                    {
                        LineType = invElement.LINE_TYPE,
                        UnitPrice = invElement.UNIT_SELLING_PRICE,
                        OrderQuantity = invElement.ORDER_QUANTITY,
                        ShipQty = invElement.SHIP_QTY,
                        ProductDescription = invElement.ITEM_DESCRIPTION,
                        BackorderQuantity = invElement.BACKORDER_QTY
                    });
                }

                result.Add(invoice);
            }
            return result;
        }

        public IList<InvoiceInfo> GetInvoicesForBdac(string orderNumber)
        {
            var invoiceDetailsList = ReadList(GetInvoicesForBdacProcedureName,
                                                   CommandType.StoredProcedure,
                                                   reader => new
                                                   {
                                                       INVOICE_NUMBER = reader.IsDBNull(0) ? String.Empty : reader.GetString(0),
                                                       INVOICE_DATE = reader.IsDBNull(1) ? (DateTime?)null : reader.GetDateTime(1),
                                                       INVOICE_TOTAL = reader.IsDBNull(2) ? 0 : reader.GetDecimal(2)
                                                   },
                                                   new OracleParameter("p_order_number", orderNumber),
                                                   new OracleParameter("x_recordset", OracleDbType.RefCursor, ParameterDirection.Output));

            var result = new List<InvoiceInfo>();

            if (invoiceDetailsList == null || !invoiceDetailsList.Any())
            {
                return result;
            }

            result.AddRange(invoiceDetailsList.Select(inv => new InvoiceInfo()
            {
                InvoiceNumber = inv.INVOICE_NUMBER.ToString(),
                InvoiceTotal = inv.INVOICE_TOTAL,
                InvoiceDate = inv.INVOICE_DATE
            }));

            return result;
        }
    }
}
