﻿using System;
using System.Collections.Generic;
using System.Data;
using BDA.ERP.Data.Contracts;
using BDA.OrderService.Objects;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace BDA.ERP.Data
{
    public class SkuHistoryRepository : ErpRepository, ISkuHistoryRepository
    {
        public int GetSkuOrdersCount(string sku)
        {
            var parameters = new[]
                                 {
                                     new OracleParameter("p_qty", OracleDbType.Decimal, ParameterDirection.ReturnValue),
                                     new OracleParameter("p_sku", OracleDbType.Varchar2, sku, ParameterDirection.Input)
                                 };

            using (var cmd = new OracleCommand("BDAWEB.XXBDA_ORDER_INV_MOVE_BY_SKU.Get_SKU_Orders_Count", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(parameters);

                cmd.ExecuteNonQuery();

                var val = (OracleDecimal)cmd.Parameters["p_qty"].Value;
                return val.IsNull ? 0 : Convert.ToInt32(Math.Truncate(val.Value));
            }
        }

        public IList<SkuOrder> GetSkuOrders(string sku, int startRecord, int endRecord)
        {
            return ReadList("BDAWEB.XXBDA_ORDER_INV_MOVE_BY_SKU.Get_SKU_Orders",
                            CommandType.StoredProcedure,
                            reader =>
                            new SkuOrder
                            {
                                Sku = reader.IsDBNull(0) ? string.Empty : reader.GetString(0),
                                OrderNo = reader.IsDBNull(1) ? 0 : Convert.ToInt32(reader.GetDecimal(1)),
                                OrderDate = reader.IsDBNull(2) ? string.Empty : reader.GetDateTime(2).ToString("MM/dd/yyyy"),
                                ShippedQty = reader.IsDBNull(3) ? 0 : Convert.ToInt32(reader.GetDecimal(3)),
                            },
                            new OracleParameter("p_sku", OracleDbType.Varchar2, sku, ParameterDirection.Input),
                            new OracleParameter("p_start_record", OracleDbType.Int32, startRecord, ParameterDirection.Input),
                            new OracleParameter("p_end_record", OracleDbType.Int32, endRecord, ParameterDirection.Input),
                            new OracleParameter("x_recordset", OracleDbType.RefCursor, ParameterDirection.Output));
        }

        public int GetSkuInventoryMovesCount(string sku)
        {
            var parameters = new[]
                                 {
                                     new OracleParameter("p_qty", OracleDbType.Decimal, ParameterDirection.ReturnValue),
                                     new OracleParameter("p_sku", OracleDbType.Varchar2, sku, ParameterDirection.Input)
                                 };

            using (var cmd = new OracleCommand("BDAWEB.XXBDA_ORDER_INV_MOVE_BY_SKU.Get_SKU_Invent_Move_Count", Connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(parameters);

                cmd.ExecuteNonQuery();

                var val = (OracleDecimal)cmd.Parameters["p_qty"].Value;
                return val.IsNull ? 0 : Convert.ToInt32(Math.Truncate(val.Value));
            }
        }

        public IList<SkuInventoryMove> GetSkuInventoryMoves(string sku, int startRecord, int endRecord)
        {
            return ReadList("BDAWEB.XXBDA_ORDER_INV_MOVE_BY_SKU.Get_SKU_Invent_Move",
                            CommandType.StoredProcedure,
                            reader =>
                            new SkuInventoryMove
                            {
                                Sku = reader.IsDBNull(1) ? string.Empty : reader.GetString(1),
                                TransactionDate = reader.IsDBNull(2) ? string.Empty : reader.GetString(2),
                                TransactionQty = reader.IsDBNull(3) ? 0 : Convert.ToInt32(reader.GetDecimal(3)),
                                InventoryMovement = reader.IsDBNull(4) ? string.Empty : reader.GetString(4),
                                InventoryMovementType = reader.IsDBNull(5) ? string.Empty : reader.GetString(5),
                                Reference = reader.IsDBNull(6) ? string.Empty : reader.GetString(6),
                            },
                            new OracleParameter("p_sku", OracleDbType.Varchar2, sku, ParameterDirection.Input),
                            new OracleParameter("p_start_record", OracleDbType.Int32, startRecord, ParameterDirection.Input),
                            new OracleParameter("p_end_record", OracleDbType.Int32, endRecord, ParameterDirection.Input),
                            new OracleParameter("x_recordset", OracleDbType.RefCursor, ParameterDirection.Output));
        }
    }
}
