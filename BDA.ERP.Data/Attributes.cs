﻿using Oracle.ManagedDataAccess.Client;
using System;


namespace BDA.ERP.Data
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MappingAttribute : Attribute
    {
        public MappingAttribute(string name, OracleDbType dbType)
        {
            Name = name;
            DbType = dbType;
        }

        public string Name { get; set; }
        public bool IsNullable { get; set; }
        public OracleDbType DbType { get; set; }
        public int? Size { get; set; }
    }
}