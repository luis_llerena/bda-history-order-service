﻿using System.Data;
using BDA.DataAccess.Oracle;
using BDA.ERP.Data.Contracts;

namespace BDA.ERP.Data
{
    public class GeographyRepository : ErpRepository, IGeographyRepository
    {
//        public IList<Address> FindAddressesUS(string state, string city, string postalCode, out bool hasMultipleCounties)
//        {
//            bool hmc = false;

//            var addresses = ReadEx("BDAWEB.OI_GEOGRAPHY.FIND_MATCHING_ADDRESSES_US", CommandType.StoredProcedure,
//                     reader => new Address
//                                   {
//                                       Country = reader.GetString(0),
//                                       State = reader.GetString(1),
//                                       County = reader.GetString(2),
//                                       City = reader.GetString(3),
//                                       ZipCode = reader.GetString(4)
//                                   },
//                     parameters =>
//                         {
//                             hmc = ((OracleDecimal) (parameters["x_has_multiple_counties"]).Value).ToInt32() == 1;
//                         },
//                     new OracleParameter("p_state", state),
//                     new OracleParameter("p_city", city),
//                     new OracleParameter("p_postal_code", postalCode),
//                     new OracleParameter("x_has_multiple_counties", OracleDbType.Int32, ParameterDirection.Output),
//                     new OracleParameter("x_recordset", OracleDbType.RefCursor, ParameterDirection.Output)
//                );

//            hasMultipleCounties = hmc;

//            return addresses;
//        }

//        public AddressValidationResult ValidateAddress(Address address)
//        {
//            var location = new Location
//                {
//                    Country = address.Country,
//                    County = address.County,
//                    State = address.State,
//                    Province = address.State,
//                    City = address.City,
//                    PostalCode = address.ZipCode
//                };

//            var query = new OracleQuery()
//                .AddObjectParameter("p_address", location, ParameterDirection.Input)
//                .AddParameter("x_is_valid", OracleDbType.Int32)
//                .AddParameter("x_failed_field", OracleDbType.Varchar2, 30)
//                .AddParameter("x_allowed_values", OracleDbType.RefCursor);

//            var parameters = query.ExecuteProcedure(Connection, "BDAWEB.OI_GEOGRAPHY.VALIDATE_ADDRESS");

//            var result = new AddressValidationResult
//                {
//                    IsValidAddress = ((OracleDecimal) parameters["x_is_valid"].Value).Value == 1
//                };

//            if (!result.IsValidAddress)
//            {
//                result.InvalidField = ((OracleString) parameters["x_failed_field"].Value).Value;
//                result.ValidFieldValues = new List<string>();

//                using (var cursor = ((OracleRefCursor) parameters["x_allowed_values"].Value))
//                using (var r = cursor.GetDataReader())
//                {
//                    while (r.Read())
//                    {
//                        result.ValidFieldValues.Add(r.GetString(0));
//                    }
//                }
//            }

//            return result;
//        }

//        public IList<Geography> GetAllGeographies()
//        {
//            return
//                ReadList(@"
//SELECT GEOGRAPHY_ID, GEOGRAPHY_TYPE, GEOGRAPHY_NAME, GEOGRAPHY_CODE, PARENT_ID
//FROM AR.HZ_GEOGRAPHIES
//LEFT JOIN AR.HZ_HIERARCHY_NODES ON CHILD_ID=GEOGRAPHY_ID AND LEVEL_NUMBER = 1
//WHERE GEOGRAPHY_TYPE IN ('COUNTRY', 'STATE', 'PROVINCE', 'COUNTY', 'CITY', 'POSTAL_CODE')
//--AND rownum <= 20000
//ORDER BY GEOGRAPHY_ID
//"
//                         ,
//                         CommandType.Text,
//                         reader => new Geography
//                             {
//                                 Id = Convert.ToInt64(reader.GetDecimal(0)),
//                                 GeographyType = ConvertStringToGeographyType(reader.GetString(1)),
//                                 Name = reader.GetString(2),
//                                 Code = reader.IsDBNull(3) ? null : reader.GetString(3),
//                                 ParentId = reader.IsDBNull(4) ? (long?) null : Convert.ToInt64(reader.GetDecimal(4))
//                             });
//        }

//        private static GeographyType ConvertStringToGeographyType(string type)
//        {
//            switch (type)
//            {
//                case "COUNTRY":
//                    return GeographyType.Country;
//                case "STATE":
//                    return GeographyType.State;
//                case "PROVINCE":
//                    return GeographyType.Province;
//                case "COUNTY":
//                    return GeographyType.County;
//                case "CITY":
//                    return GeographyType.City;
//                case "POSTAL_CODE":
//                    return GeographyType.PostalCode;
//                default:
//                    throw new ArgumentException("Unknown Geography Type", "type");
//            }
//        }
    }
}
