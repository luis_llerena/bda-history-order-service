﻿using System.Data;
using System.Linq;
using BDA.ERP.Data.Contracts;
using Oracle.ManagedDataAccess.Client;

namespace BDA.ERP.Data
{
    public class SalesOrderRepository : ErpRepository, ISalesOrderRepository
    {
        public bool CheckSalesOrder(string number)
        {
            long parsedNumber;
            System.Int64.TryParse(number, out parsedNumber);
            //AR
            const string query = @"SELECT * FROM ONT.OE_ORDER_HEADERS_ALL
                                   WHERE ORDER_NUMBER = :p_order_number";

            return ReadList(query,
                            CommandType.Text,
                            reader => reader,
                            new OracleParameter("p_order_number", OracleDbType.Int64, parsedNumber, ParameterDirection.Input)).Any();
        }
    }
}
