﻿using System;
using System.Collections.Generic;
using System.Data;
using BDA.DataAccess.Oracle;
using BDA.ERP.Data.Contracts.Exceptions;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace BDA.ERP.Data
{
    public class ErpRepository : OracleRepository
    {
        #region Consts

        protected const string MODULE = "TCA_V2_API";

        protected const string INIT_MSG_LIST_PARAM_NAME = "p_init_msg_list";
        protected const string STATUS_PARAM_NAME = "x_return_status";
        protected const string MSG_DATA_PARAM_NAME = "x_msg_data";
        protected const string MSG_COUNT_PARAM_NAME = "x_msg_count";

        #endregion Consts

        #region Methods

        protected int GetVersionNumber(string tableName, string pkColumnName, int id)
        {
            string sql = string.Format("SELECT OBJECT_VERSION_NUMBER FROM {0} WHERE {1}=:id", tableName, pkColumnName);

            using (var cmd = new OracleCommand(sql, Connection))
            {
                cmd.Parameters.Add("id", OracleDbType.Decimal, id, ParameterDirection.Input);

                var res = cmd.ExecuteScalar();

                return Convert.ToInt32(res);
            }
        }

        protected void ProcessReturnStatus(IDictionary<string, OracleParameter> parameters)
        {
            var status = (OracleString)parameters[STATUS_PARAM_NAME].Value;

            if (status.Value == "S")
                return;

            var messageCount = ((OracleDecimal)parameters[MSG_COUNT_PARAM_NAME].Value).ToInt32();
            if (messageCount == 0)
            {
                throw new InvalidOperationException("Oracle hasn't returned any errors.");
            }

            string message = string.Empty;
            if (messageCount == 1)
            {
                message = ((OracleString)parameters[MSG_DATA_PARAM_NAME].Value).Value;
            }
            else
            {
                // execute stored procedure to retrieve errors.
                Execute("BDAWEB.GET_MESSAGE", CommandType.StoredProcedure,
                        pars =>
                        {
                            var value = (OracleString)pars["x_result"].Value;
                            message = value.IsNull ? string.Empty : value.Value.Trim();
                        },
                        new OracleParameter("x_result", OracleDbType.Varchar2, 2000, null, ParameterDirection.Output));
            }

            switch (status.Value)
            {
                case "U":
                    throw new UnexpectedErpException(message);
                case "E":
                    throw new ExpectedErpException(message);
            }
        }

        #endregion Methods
    }
}
