﻿using BDA.Commerce.WebServices.Data.Contracts;
using BDA.DataAccess;
using BDA.ERP.Data.Contracts;
using BDA.OrderService.Objects.Order;
using BDA.OrdersService.Services.Contracts;

namespace BDA.OrdersService.Services
{
    public class OrderHistoryService : IOrderHistoryService
    {
        private readonly IDataContext<IErpRepository> _erpContext;
        private readonly IDataContext<IBdaCommerceRepository> _bdacContext;
        private readonly List<string> _mpbillingProfiles = new List<string> { "MER-MPWPO", "ATT-TCARBILLING" };

        public OrderHistoryService(IDataContext<IErpRepository> erpContext, IDataContext<IBdaCommerceRepository> bdacContext)
        {
            _erpContext = erpContext;
            _bdacContext = bdacContext;
        }

        public IList<Invoice> GetInvoices(string orderNumber)
        {
            return _erpContext.GetRepository<IOrderHistoryRepository>().GetInvoicesByOrderNumber(orderNumber);
        }

        public IList<InvoiceInfo> GetInvoicesInfo(string orderNumber)
        {
            return _erpContext.GetRepository<IOrderHistoryRepository>().GetInvoicesForBdac(orderNumber);
        }

        public Order GetOrder(string orderNumber, bool? isMp, string locale)
        {
            var repo = _erpContext.GetRepository<IOrderHistoryRepository>();
            var bdacRepo = _bdacContext.GetRepository<IBdacItemRepository>();

            var order = repo.GetOrder(orderNumber);

            if (order == null)
                return null;

            if (isMp.HasValue && isMp.Value != _mpbillingProfiles.Contains(order.BillingProfile))
            {
                return null;
            }

            var rawItems = repo.GetOrderItems(new[] { order.OrderNumber });
            var bdacItemVariants = bdacRepo.GetItemVariants(rawItems.Select(i => i.Sku));

            return TranslateRawAndCombine(new[] { order }, rawItems, bdacItemVariants, locale).First();           
        }

        public Order GetOrderByBdacOrderNumber(string orderNumber, bool? isMp, string locale)
        {
            var repo = _erpContext.GetRepository<IOrderHistoryRepository>();
            var bdacRepo = _bdacContext.GetRepository<IBdacItemRepository>();

            var order = repo
                .GetOrderByBdacOrderNumber(orderNumber);

            if (order == null)
                return null;

            if (isMp.HasValue && isMp.Value != _mpbillingProfiles.Contains(order.BillingProfile))
            {
                return null;
            }
            //order = new RawOrder()
            //{
            //    AccountId = 1,
            //    AccountName = "fff",
            //    OrderNumber = "4234234",
            //    TaxTotal = 0,
            //    ShippingTotal = 0
            //};
            var rawItems = repo.GetOrderItems(new[] { order.OrderNumber });

            var rawItems2 = new List<RawOrderItem>();
            //rawItems2.Add(new RawOrderItem()
            //{
            //    Sku = "1448184-00"
            //});
            var bdacItemVariants = bdacRepo.GetItemVariants(rawItems.Select(i => i.Sku));

            return TranslateRawAndCombine(new[] { order }, rawItems, bdacItemVariants, locale).First();
        }

        public IList<Order> GetOrders(int? accountId, string email, string program, OrderQueryStatus status, DateTime? fromDate, DateTime? toDate, int startRow, int rowCount, string salesperson, bool? isMp, string customerPo, string zipCode, string locale)
        {
            var repo = _erpContext.GetRepository<IOrderHistoryRepository>();
            var bdacRepo = _bdacContext.GetRepository<IBdacItemRepository>();

            var orders = repo
                .GetOrders(accountId, email, program, OrderQueryStatusToString(status), fromDate, toDate, startRow + 1, startRow + rowCount, salesperson, isMp, customerPo, zipCode);

            if (orders.Count == 0)
                return new Order[0];

            var rawItems = repo.GetOrderItems(orders.Select(o => o.OrderNumber).ToArray());
            var bdacItems = bdacRepo.GetItemVariants(rawItems.Select(i => i.Sku));

            return TranslateRawAndCombine(orders, rawItems, bdacItems, locale);
        }

        public int GetOrdersCount(int? accountId, string email, string program, OrderQueryStatus status, DateTime? fromDate, DateTime? toDate, string salesperson, bool? isMp, string customerPo, string zipCode)
        {
            var repo = _erpContext.GetRepository<IOrderHistoryRepository>();
            var result = repo.GetOrdersCount(accountId, email, program, OrderQueryStatusToString(status), fromDate, toDate, salesperson, isMp, customerPo, zipCode);
            return result;
        }

        private IList<Order> TranslateRawAndCombine(IEnumerable<RawOrder> rawOrders, IEnumerable<RawOrderItem> rawItems, IEnumerable<BdacItemVariant> bdacItemVariants, string locale)
        {
            var orders = rawOrders
                .Select(raw => new Order
                {
                    OrderNumber = raw.OrderNumber,
                    ShopperEmail = raw.ShopperEmail,
                    OrderDate = raw.OrderDate,
                    StatusDescription = raw.StatusDescription,
                    ShipTo = raw.ShipTo,
                    BillTo = raw.BillTo,
                    Subtotal = 0, // will be calculated later
                    Freight = raw.ShippingTotal,
                    RushFee = raw.RushTotal,
                    Tax = raw.TaxTotal,
                    OrderTotal = 0, // will be calculated later
                    ProgramCode = raw.ProgramCode,
                    BillingProfile = raw.BillingProfile,
                    Currency = raw.Currency
                })
                .ToList();

            var grouping = rawItems
                // group items into shipments
                .GroupBy(item => new
                {
                    item.OrderNumber,
                    item.ShipDate,
                    item.ShippingMethod,
                    item.Carrier,
                    Status = TranslateStatus(item.Status),
                    item.TrackingNumber,
                    item.ScacCode,
                    item.FreightCode,
                    item.DeliveryName
                })
                //.Where(sg => sg.Key.Status != ShipmentStatus.Cancelled)
                // group shipments into orders
                .GroupBy(shipGroup => shipGroup.Key.OrderNumber)
                .Select(orderGroup => new
                {
                    OrderNumber = orderGroup.Key,
                    Shipments = orderGroup.Select(shipGroup => new Shipment
                    {
                        ShipDate = shipGroup.Key.ShipDate,
                        ShippingMethod = shipGroup.Key.ShippingMethod,
                        Status = shipGroup.Key.Status,
                        TrackingNumber = shipGroup.Key.TrackingNumber,
                        TrackingUrl = GetTrackingUrl(shipGroup.Key.ScacCode, shipGroup.Key.TrackingNumber),
                        Lines = shipGroup.Select(item => new OrderLine
                        {
                            Sku = item.Sku,
                            Description = item.Description,
                            Quantity = GetAppropriateQuantity(item),
                            UnitPrice = item.UnitPrice,
                            VendorCustomizationUrl = item.VendorCustomizationUrl
                        }).ToList()
                    }).ToList()
                }).ToList();

            // calculate totals
            foreach (var line in grouping.SelectMany(g => g.Shipments).SelectMany(s => s.Lines))
            {
                line.ExtendedPrice = line.UnitPrice * line.Quantity;
            }

            foreach (var order in orders)
            {
                var group = grouping.FirstOrDefault(g => g.OrderNumber == order.OrderNumber);
                order.Shipments = group == null
                    ? new List<Shipment>()
                    : group.Shipments;
                order.Subtotal = order.Shipments.SelectMany(s => s.Lines).Sum(l => l.ExtendedPrice);
                order.OrderTotal = order.Subtotal + order.Freight + order.RushFee + order.Tax;
            }

            var bdacItemVariantList = bdacItemVariants.ToList();

            // translate according to locale if possible

            if (locale != null)
            {
                foreach (var line in orders.SelectMany(o => o.Shipments).SelectMany(s => s.Lines))
                {
                    var localization = bdacItemVariantList
                        .FirstOrDefault(i => i.Sku == line.Sku)?
                        .Localizations
                        .FirstOrDefault(l => string.Equals(l.Locale.LocaleName, locale, StringComparison.InvariantCultureIgnoreCase));

                    if (localization != null)
                    {
                        line.Description = localization.Title;
                    }
                }
            }

            // fill estimated ship date

            bdacItemVariantList = bdacItemVariantList
                .Where(i => i.EstimatedShipDate != null && i.EstimatedShipDate > DateTime.Today
                            && (i.StockType == BdacStockType.MadeToOrder
                                || GetVariantStockStatus(i) == BdacStockStatus.BackOrder
                                || i.IsPreorder))
                .ToList();

            // all lines in all orders, 
            // in shipments that are not shipped, cancelled or completed (i.e. processing or backordered)
            foreach (var line in orders
                .SelectMany(o => o.Shipments)
                .Where(s => s.Status < ShipmentStatus.Shipped)
                .SelectMany(s => s.Lines))
            {
                var bdacItemVariant = bdacItemVariantList
                    .Where(i => i.Sku == line.Sku)
                    .OrderBy(i => i.EstimatedShipDate)
                    .FirstOrDefault();

                if (bdacItemVariant != null)
                {
                    line.EstimatedShipDate = bdacItemVariant.EstimatedShipDate;
                }
            }

            return orders;
        }

        private static BdacStockStatus GetVariantStockStatus(BdacItemVariant variant)
        {
            var uniqueStatuses = variant.ItemStocks.Select(s => s.StockStatus).Distinct().ToArray();

            if (uniqueStatuses.Contains(BdacStockStatus.Available))
                return BdacStockStatus.Available;

            if (uniqueStatuses.Contains(BdacStockStatus.BackOrder))
                return BdacStockStatus.BackOrder;

            if (uniqueStatuses.Contains(BdacStockStatus.RunOut))
                return BdacStockStatus.RunOut;

            return BdacStockStatus.Unavailable;
        }

        private static int GetAppropriateQuantity(RawOrderItem item)
        {
            return item.DeliveryShippedQuantity ?? item.DeliveryRequestedQuantity ?? item.LineOrderQuantity;
        }

        private static ShipmentStatus TranslateStatus(string erpStatus)
        {
            var s = ((erpStatus ?? "") + " ")[0];
            switch (s)
            {
                case 'B':
                    return ShipmentStatus.Backordered;
                case 'C':
                    return ShipmentStatus.Shipped;
                case 'D':
                    return ShipmentStatus.Cancelled;
                case 'X':
                    return ShipmentStatus.Complete;
                default:
                    return ShipmentStatus.Processing;
            }
        }

        private string OrderQueryStatusToString(OrderQueryStatus status)
        {
            switch (status)
            {
                case OrderQueryStatus.All:
                    return "ALL";
                case OrderQueryStatus.Open:
                    return "OPEN";
                case OrderQueryStatus.Cancelled:
                    return "CANCEL";
            }
            throw new ArgumentException("Invalid value", "status");
        }

        private string GetTrackingUrl(string scac, string number)
        {
            if (string.IsNullOrEmpty(number))
                return string.Empty;

            //var tr = _localContext.GetRepository<ITrackingUrlTemplateRepository>();
            //var template = tr.GetTrackingUrl(scac);

            //if (string.IsNullOrWhiteSpace(template))
            //    return string.Empty;

            //return string.Format(template, number);
            return String.Empty;
        }
    }
}
