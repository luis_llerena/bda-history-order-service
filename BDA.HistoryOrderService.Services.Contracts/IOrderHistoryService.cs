﻿
using BDA.OrderService.Objects.Order;

namespace BDA.OrdersService.Services.Contracts
{
    public interface IOrderHistoryService
    {
        Order GetOrder(string orderNumber, bool? isMp, string locale);
        Order GetOrderByBdacOrderNumber(string orderNumber, bool? isMp, string locale);
        IList<Order> GetOrders(int? accountId, string email, string program, OrderQueryStatus status, DateTime? fromDate, DateTime? toDate, int startRow, int rowCount, string salesperson, bool? isMp, string customerPo, string zipCode, string locale);
        int GetOrdersCount(int? accountId, string email, string program, OrderQueryStatus status, DateTime? fromDate, DateTime? toDate, string salesperson, bool? isMp, string customerPo, string zipCode);
        IList<Invoice> GetInvoices(string orderNumber);
        IList<InvoiceInfo> GetInvoicesInfo(string orderNumber);
    }
}
