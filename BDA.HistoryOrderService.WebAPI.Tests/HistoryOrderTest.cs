using BDA.OrderService.Objects.Order;
using BDA.OrdersService.Services.Contracts;
using NSubstitute;
using System.Collections.Generic;
using Xunit;

namespace BDA.OrderService.WebAPI.Tests
{
    public class HistoryOrderTest
    {
        [Fact]
        public void GetNotEmptyInvoiceTest()
        {
            List<Invoice> invoices = new List<Invoice>();
            invoices.Add(new Invoice()
            {
                InvoiceNumber = "12345",
                InvoiceTotal = 85,
                InvoiceDate = new System.DateTime(2022, 2, 15),
                TaxTotal = 15,
                OrderNumber = "554532"
            });

            var historyService = Substitute.For<IOrderHistoryService>();
            historyService.GetInvoices("12345").Returns(invoices);
            Assert.NotEmpty(invoices);
        }

        [Fact]
        public void GetEmptyInvoiceTest()
        {
            List<Invoice> invoices = new List<Invoice>();
            var historyService = Substitute.For<IOrderHistoryService>();
            historyService.GetInvoices("5929588").Returns(invoices);
            Assert.Empty(invoices);
        }

        [Fact]
        public void GetOrderTest()
        {
            Order order = new Order();
            var historyService = Substitute.For<IOrderHistoryService>();
            historyService.GetOrder("5929588",false,"us").Returns(order);
            Assert.Null(order);
        }
    }
}