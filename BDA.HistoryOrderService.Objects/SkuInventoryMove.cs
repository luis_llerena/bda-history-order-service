﻿namespace BDA.OrderService.Objects
{
    public class SkuInventoryMove
    {
        public string Sku { get; set; }

        public string TransactionDate { get; set; }

        public int TransactionQty { get; set; }

        public string InventoryMovement { get; set; }

        public string InventoryMovementType { get; set; }

        public string Reference { get; set; }
    }
}
