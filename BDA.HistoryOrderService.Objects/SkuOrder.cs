﻿
namespace BDA.OrderService.Objects
{

    public class SkuOrder
    {
        public string Sku { get; set; }

        public int OrderNo { get; set; }

        public string OrderDate { get; set; }

        public int ShippedQty { get; set; }
    }
}
