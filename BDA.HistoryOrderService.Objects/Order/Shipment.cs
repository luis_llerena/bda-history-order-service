using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BDA.OrderService.Objects.Order
{
    public class Shipment
    {
        [DataMember]
        public DateTime? ShipDate { get; set; }
        [DataMember]
        public string ShippingMethod { get; set; }
        [DataMember]
        public ShipmentStatus Status { get; set; }
        [DataMember]
        public string TrackingNumber { get; set; }
        [DataMember]
        public string TrackingUrl { get; set; }
        [DataMember]
        public List<OrderLine> Lines { get; set; }
    }
}