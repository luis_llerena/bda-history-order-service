using System;

namespace BDA.OrderService.Objects.Order
{
    public class RawOrder
    {
        public string OrderNumber { get; set; }
        public string StatusDescription { get; set; }
        public string OrderType { get; set; }
        public DateTime OrderDate { get; set; }
        public int AccountId { get; set; }
        public int PartyId { get; set; }
        public string AccountName { get; set; }
        public string ProgramCode { get; set; }
        public string ShopperEmail { get; set; }
        public Address BillTo { get; set; }
        public Address ShipTo { get; set; }
        public string ShipMethod { get; set; }
        public string Carrier { get; set; }
        public decimal ShippingTotal { get; set; }
        public decimal RushTotal { get; set; }
        public decimal TaxTotal { get; set; }
        public decimal Discount { get; set; }
        public string BillingProfile { get; set; }
        public string Currency { get; set; }
    }
}