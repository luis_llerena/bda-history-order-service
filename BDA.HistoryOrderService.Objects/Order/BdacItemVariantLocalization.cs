﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BDA.OrderService.Objects.Order
{
    public class BdacItemVariantLocalization
    {
        [Key]
        public Guid ItemVariantLocalizationId { get; set; }
        public Guid ItemVariantId { get; set; }
        public Guid LocaleId { get; set; }
        public string Title { get; set; }
        public BdacItemVariant ItemVariant { get; set; }
        public BdacLocale Locale { get; set; }
    }
}