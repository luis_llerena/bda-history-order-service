using System.Runtime.Serialization;

namespace BDA.OrderService.Objects.Order
{
    public enum ShipmentStatus
    {
        [EnumMember]
        Processing,
        [EnumMember]
        Backordered,
        [EnumMember]
        Shipped,
        [EnumMember]
        Cancelled,
        [EnumMember]
        Complete,
    }
}