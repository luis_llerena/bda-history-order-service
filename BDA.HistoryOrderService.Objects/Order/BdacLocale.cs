﻿using System;

namespace BDA.OrderService.Objects.Order
{
    public class BdacLocale
    {
        public Guid LocaleId { get; set; }
        public string LocaleName { get; set; }
    }
}