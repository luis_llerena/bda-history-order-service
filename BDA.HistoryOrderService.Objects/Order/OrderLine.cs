using System;
using System.Runtime.Serialization;

namespace BDA.OrderService.Objects.Order
{
    public class OrderLine
    {
        [DataMember]
        public string Sku { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int Quantity { get; set; }
        [DataMember]
        public decimal UnitPrice { get; set; }
        [DataMember]
        public decimal ExtendedPrice { get; set; }
        [DataMember]
        public DateTime? EstimatedShipDate { get; set; }
        [DataMember]
        public string VendorCustomizationUrl { get; set; }
    }
}