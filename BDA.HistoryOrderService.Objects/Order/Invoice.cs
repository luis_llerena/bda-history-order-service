﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BDA.OrderService.Objects.Order
{    
    public class Invoice
    {
        [DataMember]
        public string OrderNumber { get; set; }
        [DataMember]
        public string InvoiceNumber { get; set; }
        [DataMember]
        public int LineNumber { get; set; }
        [DataMember]
        public Address ShipTo { get; set; }
        [DataMember]
        public Address BillTo { get; set; }
        [DataMember]
        public DateTime? InvoiceDate { get; set; }
        [DataMember]
        public string Terms { get; set; }
        [DataMember]
        public DateTime? ShipDate { get; set; }
        [DataMember]
        public DateTime? DueDate { get; set; }
        [DataMember]
        public string PONumber { get; set; }
        [DataMember]
        public string CarrierName { get; set; }
        [DataMember]
        public decimal TaxTotal { get; set; }
        [DataMember]
        public decimal FreightTotal { get; set; }
        [DataMember]
        public decimal SpecialCharge { get; set; }
        [DataMember]
        public decimal OrderDiscount { get; set; }
        [DataMember]
        public decimal BalanceDue { get; set; }
        [DataMember]
        public decimal InvoiceTotal { get; set; }
        [DataMember]
        public IList<InvoiceLine> InvoiceDetailLines;
    }
}
