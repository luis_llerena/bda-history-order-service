﻿namespace BDA.OrderService.Objects.Order
{
    public enum BdacStockType : short
    {
        Inventory = 0,
        MadeToOrder = 1,
        Both = 2
    }
}
