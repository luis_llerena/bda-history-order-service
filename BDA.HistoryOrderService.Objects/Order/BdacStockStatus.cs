﻿namespace BDA.OrderService.Objects.Order
{
    public enum BdacStockStatus
    {
        Unavailable = 0,
        Available = 1,
        BackOrder = 3,
        RunOut = 4
    }
}