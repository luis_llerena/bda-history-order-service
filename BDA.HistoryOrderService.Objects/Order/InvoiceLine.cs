using System.Runtime.Serialization;

namespace BDA.OrderService.Objects.Order
{
    public class InvoiceLine
    {
        [DataMember]
        public string LineType { get; set; }
        [DataMember]
        public decimal OrderQuantity { get; set; }
        [DataMember]
        public decimal ShipQty { get; set; }
        [DataMember]
        public decimal BackorderQuantity { get; set; }
        [DataMember]
        public string ProductDescription { get; set; }
        [DataMember]
        public decimal UnitPrice { get; set; }
    }
}