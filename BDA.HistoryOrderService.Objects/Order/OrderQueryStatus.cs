using System.Runtime.Serialization;

namespace BDA.OrderService.Objects.Order
{
    public enum OrderQueryStatus
    {
        [EnumMember]
        All,
        [EnumMember]
        Open,
        [EnumMember]
        Cancelled,
    }
}