﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BDA.OrderService.Objects.Order
{
    public class BdacItemStock
    {
        [Key]
        public Guid ItemStockId { get; set; }
        public Guid ItemVariantId { get; set; }
        public BdacStockStatus StockStatus { get; set; }
        public BdacItemVariant ItemVariant { get; set; }
    }
}