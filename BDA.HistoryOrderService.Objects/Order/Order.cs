﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BDA.OrderService.Objects.Order
{
    public class Order
    {
        [DataMember]
        public string OrderNumber { get; set; }
        [DataMember]
        public string ShopperEmail { get; set; }
        [DataMember]
        public DateTime OrderDate { get; set; }
        [DataMember]
        public string StatusDescription { get; set; }
        [DataMember]
        public Address ShipTo { get; set; }
        [DataMember]
        public Address BillTo { get; set; }
        [DataMember]
        public List<Shipment> Shipments { get; set; }
        [DataMember]
        public decimal Subtotal { get; set; }
        [DataMember]
        public decimal Freight { get; set; }
        [DataMember]
        public decimal RushFee { get; set; }
        [DataMember]
        public decimal Tax { get; set; }
        [DataMember]
        public decimal OrderTotal { get; set; }
        [DataMember]
        public string ProgramCode { get; set; }
        [DataMember]
        public string BillingProfile { get; set; }
        [DataMember]
        public string Currency { get; set; }
    }
}