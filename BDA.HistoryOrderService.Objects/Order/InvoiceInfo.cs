﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BDA.OrderService.Objects.Order
{
    //[DataContract(Namespace = Namespace.Name)]
    public class InvoiceInfo
    {
        [DataMember]
        public string InvoiceNumber { get; set; }

        [DataMember]
        public DateTime? InvoiceDate { get; set; }

        [DataMember]
        public decimal InvoiceTotal { get; set; }

        [DataMember]
        public string InvoiceUrl { get; set; }
    }
}
