using System;

namespace BDA.OrderService.Objects.Order
{
    public class RawOrderItem
    {
        // order
        public string OrderNumber { get; set; }

        // shipment
        public DateTime? ShipDate { get; set; }
        public string ShippingMethod { get; set; }
        public string Carrier { get; set; }
        public string Status { get; set; }
        public string TrackingNumber { get; set; }
        public string ScacCode { get; set; }
        public string FreightCode { get; set; }
        public string DeliveryName { get; set; }

        // line
        public string Sku { get; set; }
        public string Description { get; set; }
        public int LineOrderQuantity { get; set; }
        public int LineShipQuantity { get; set; }
        public int? DeliveryRequestedQuantity { get; set; }
        public int? DeliveryShippedQuantity { get; set; }
        public decimal UnitPrice { get; set; }
        public int UomConversionFactor { get; set; }
        public string VendorCustomizationUrl { get; set; }
    }
}