﻿using BDA.OrderService.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BDA.OrderService.Objects.Order
{
    public class BdacItemVariant
    {
        [Key]
        public Guid ItemVariantId { get; set; }
        public string Sku { get; set; }
        public BdacStockType StockType { get; set; }
        public bool IsPreorder { get; set; }
        public int LeadTimeDays { get; set; }
        public Guid ItemId { get; set; }
        public DateTime? EstimatedShipDate { get; set; }
        public BdacItem Item { get; set; }
        public List<BdacItemVariantLocalization> Localizations { get; set; }
        public List<BdacItemStock> ItemStocks { get; set; }
    }
}