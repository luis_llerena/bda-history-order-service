using System.Runtime.Serialization;

namespace BDA.OrderService.Objects.Order
{
    public class Address
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Street { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string Country2DigitCode { get; set; }
        [DataMember]
        public string PostalCode { get; set; }
        [DataMember]
        public string State { get; set; }
    }
}