﻿using BDA.OrderService.Objects.Order;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDA.OrderService.Objects
{
    public class ItemVariant
    {
        [Key]
        public Guid ItemVariantId { get; set; }
        public Guid ItemId { get; set; }
        public string Sku { get; set; }
        public int LeadTimeDays { get; set; }
        public bool IsPreOrder { get; set; }
        public DateTime? EstimatedShipDate { get; set; }
        //public BdacStockType StockType { get; set; }
        //public BdacItemStock ItemStock { get; set; }
        //public BdacItemVariantLocalization ItemVariantLocalization { get; set; }
        //public BdacItem Item { get; set; }

    }
}
