﻿using BDA.OrderService.Objects;
using BDA.OrderService.Objects.Order;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDA.OrderService.Data
{
    public class BdaCommerceDataContext: DbContext
    {
        static BdaCommerceDataContext()
        {
            Database.SetInitializer<BdaCommerceDataContext>(null);
        }

        public BdaCommerceDataContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
        }

        public BdaCommerceDataContext(DbConnection existingConnection) : base(existingConnection, true)
        {
        }
        public virtual DbSet<ItemVariant> DbBdacItemVariants { get; set; }

        public virtual DbSet<BdacItemVariantLocalization> ItemVariantLocalizations { get; set; }

        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
