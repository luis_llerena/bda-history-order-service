﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using BDA.Commerce.WebServices.Data.Contracts;
using BDA.DataAccess;

namespace BDA.Commerce.WebServices.Data
{
    public class BdaCommerceContext : DataContext<IBdaCommerceRepository, SqlConnection>
    {
        protected override SqlConnection CreateConnection()
        {
            if (string.IsNullOrEmpty(ConnectionString))
                throw new InvalidOperationException("Connection string is not set");

            var connection = new SqlConnection(ConnectionString);

            connection.Open();

            Debug.Print("Created {0} {1} in context {2}", connection.GetType().Name, connection.GetHashCode(), GetHashCode());

            return connection;
        }

        protected override void RegisterRepositories()
        {
            RegisterRepository<IBdacItemRepository, BdacItemRepository>();
        }
    }
}