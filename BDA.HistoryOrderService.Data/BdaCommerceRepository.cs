﻿
using BDA.DataAccess;
using BDA.OrderService.Data;
using System.Data.SqlClient;

namespace BDA.Commerce.WebServices.Data
{
    public class BdaCommerceRepository : Repository<SqlConnection>
    {
        protected BdaCommerceDataContext GetNewContext()
        {
            return new BdaCommerceDataContext(Connection);
        }
    }
}