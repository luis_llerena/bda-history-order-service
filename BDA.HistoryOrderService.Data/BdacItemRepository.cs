﻿using System.Collections.Generic;
using System.Linq;
using BDA.Commerce.WebServices.Data.Contracts;
using BDA.Commerce.WebServices.Data.Mappers;
using BDA.OrderService.Objects.Order;

namespace BDA.Commerce.WebServices.Data
{
    public class BdacItemRepository : BdaCommerceRepository, IBdacItemRepository
    {
        public List<BdacItemVariant> GetItemVariants(IEnumerable<string> skuList)
        {
            var itemVariants = GetNewContext().DbBdacItemVariants
                .Where(i => skuList.Contains(i.Sku)).ToList();

            return itemVariants.Select(i => new BdacItemVariant()
            {
                ItemVariantId = i.ItemVariantId,
                Sku = i.Sku,
                ItemId = i.ItemId,
                EstimatedShipDate = i.EstimatedShipDate,
                IsPreorder = i.IsPreOrder,
                LeadTimeDays = i.LeadTimeDays

            }).ToList();
        }
    }
}