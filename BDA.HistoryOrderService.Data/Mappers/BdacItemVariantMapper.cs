﻿using System.Linq;
using BDA.OrderService.Objects;
using BDA.OrderService.Objects.Order;

namespace BDA.Commerce.WebServices.Data.Mappers
{
    public static class BdacItemVariantMapper
    {
        public static BdacItemVariant Map(this ItemVariant source)
        {
            if (source == null)
                return null;

            var result = new BdacItemVariant
            {
                ItemVariantId = source.ItemId,
                Sku = source.Sku,
                LeadTimeDays = source.LeadTimeDays,
                ItemId = source.ItemId,
                EstimatedShipDate = source.EstimatedShipDate,
            };


            return result;
        }
    }
}
