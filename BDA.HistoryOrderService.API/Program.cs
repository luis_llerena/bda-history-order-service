using BDA.DataAccess;
using BDA.ERP.Infrastructure;
using BDA.OrderService.Infrastructure;
using BDA.OrdersService.Services;
using BDA.OrdersService.Services.Contracts;
using Microsoft.OpenApi.Models;
using Serilog;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

var logger = new LoggerConfiguration()
  .ReadFrom.Configuration(builder.Configuration)
  .Enrich.FromLogContext()
  .CreateLogger();
builder.Logging.ClearProviders();
builder.Logging.AddSerilog(logger);

// Add services to the container.
builder.Services.AddScoped<IOrderHistoryService, OrderHistoryService>();

ErpInitializer.Initialize(builder.Services, new ContextConfiguration(builder.Configuration.GetConnectionString("oracleConnection")));
SqlInitializer.Initialize(builder.Services, new ContextConfiguration(builder.Configuration.GetConnectionString("bdaCommerceConnection")));
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "History Orders API",
        Description = "ASP.NET Core Web API for managing orders history items"
    });
    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
