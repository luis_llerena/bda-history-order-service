﻿using BDA.OrderService.Objects.Order;
using BDA.OrdersService.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog.Core;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;
using System.Net.Http.Headers;

namespace BDA.OrdersService.Controllers
{
    [Route("api/orders")]
    [ApiController]
  
    public class OrderController : ControllerBase
    {
        private readonly IOrderHistoryService _orderHistoryService;
        private readonly ILogger<OrderController> _logger;
        private readonly IConfiguration _configuration;

        public OrderController(IOrderHistoryService orderHistoryService, ILogger<OrderController> logger, IConfiguration configuration)
        {
            _orderHistoryService = orderHistoryService;
            _logger = logger;
            _configuration = configuration;
        }

        /// <summary>
        /// Get orders by specific criteria.
        /// </summary>
        /// <param name="accountId">ID of account</param>
        /// <param name="email">Email of the shoppper</param>
        /// <param name="status">Status of the order</param>
        /// <param name="fromDate">From date of the order</param>
        /// <param name="toDate">To date of the order</param>
        /// <param name="zipCode">Zip code of the shopper</param>
        /// <returns>List of orders</returns>
        /// <response code="200">Success - Returns the list of orders found</response>
        [Route("")]
        [HttpGet]
        public IList<Order> GetOrders(int? accountId = null, string email = null, string program = null,
           string status = null, string fromDate = null, string toDate = null, int startRow = 0,
           int rowCount = int.MaxValue, string salesperson = null, bool? isMp = null, string customerPo = null,
           string zipCode = null, string locale = null)
        {
            try
            {
                var fromD = !string.IsNullOrEmpty(fromDate) ? Convert.ToDateTime(fromDate) : (DateTime?)null;
                var toD = !string.IsNullOrEmpty(toDate) ? Convert.ToDateTime(toDate) : (DateTime?)null;
                OrderQueryStatus st;
                if (!Enum.TryParse(status, out st)) throw new ArgumentException("status");
                var result = _orderHistoryService.GetOrders(accountId, email, program, st, fromD, toD, startRow,
                    rowCount, salesperson, isMp, customerPo, zipCode, locale);
                return result;
            }
            catch(Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Get orders count by specific criteria.
        /// </summary>
        /// <param name="accountId">ID of account</param>
        /// <param name="email">Email of the shoppper</param>
        /// <param name="status">Status of the order</param>
        /// <param name="fromDate">From date of the order</param>
        /// <param name="toDate">To date of the order</param>
        /// <param name="zipCode">Zip code of the shopper</param>
        /// <returns></returns>
        [Route("count")]
        [HttpGet]
        public int GetOrdersCount(int? accountId = null, string email = null, string program = null,
           string status = null, string fromDate = null, string toDate = null, string salesperson = null,
           bool? isMp = null, string customerPo = null, string zipCode = null)
        {
            try
            {
                var fromD = !string.IsNullOrEmpty(fromDate) ? Convert.ToDateTime(fromDate) : (DateTime?)null;
                var toD = !string.IsNullOrEmpty(toDate) ? Convert.ToDateTime(toDate) : (DateTime?)null;
                OrderQueryStatus st;
                if (!Enum.TryParse(status, out st)) throw new ArgumentException("status");
                var result = _orderHistoryService.GetOrdersCount(accountId, email, program, st, fromD, toD, salesperson,
                    isMp, customerPo, zipCode);
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Get an order by number.
        /// </summary>
        /// <param name="orderNumber">Order number</param>
        /// <returns></returns>
        /// <response code="200">Success - Order detail</response>
        /// <response code="204">No order was found by the order number entered</response>
        /// <response code="500">Internal server error</response>
        [Route("{orderNumber:int}")]
        [HttpGet]
        public Order GetOrder(string orderNumber, bool? isMp = null, string locale = null)
        {
            try
            {
                return _orderHistoryService.GetOrder(orderNumber, isMp, locale);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Get an order by Bdac order number.
        /// </summary>
        /// <param name="orderNumber">BDAC order number</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="204">No order was found by the order number entered</response>
        /// <response code="500">Internal server error</response>
        [Route("bdac/{orderNumber}")]
        [HttpGet]
        public Order GetOrderByBdacOrderNumber(string orderNumber, bool? isMp = null, string locale = null)
        {
            try
            {
                return _orderHistoryService.GetOrderByBdacOrderNumber(orderNumber, isMp, locale);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Get Invoices by order number.
        /// </summary>
        /// <param name="orderNumber">Order number</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="204">No Invoices were found by the order number entered</response>
        /// <response code="500">Internal server error</response>
        [Route("{orderNumber:int}/invoices")]
        [HttpGet]
        public IList<Invoice> GetInvoices(string orderNumber)
        {
            try
            {
                return _orderHistoryService.GetInvoices(orderNumber);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Get Invoices info by order number.
        /// </summary>
        /// <param name="orderNumber">Order number</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="204">No Invoices info were found by the order number entered</response>
        /// <response code="500">Internal server error</response>
        [Route("invoices/{orderNumber:int}")]
        [HttpGet]
        public IList<InvoiceInfo> GetInvoicesInfo(string orderNumber)
        {
            var invoices = _orderHistoryService.GetInvoicesInfo(orderNumber);
            foreach (var invoice in invoices)
            {
                invoice.InvoiceUrl = GetInvoicePdfUrl(invoice.InvoiceNumber);
            }
            return invoices;
        }

        /// <summary>
        /// Get invoice pdf by invoice number.
        /// </summary>
        /// <param name="invoiceNumber">Invoice number</param>
        /// <returns></returns>
        /// <response code="200">Success</response>
        /// <response code="500">Internal server error</response>
        [Route("download/{invoiceNumber:int}")]
        [HttpGet]
        public async Task<HttpResponseMessage> DownloadInvoicePdfAsync(string invoiceNumber)
        {
            try
            {
                var client = new HttpClient();
                var url = string.Format(_configuration["InvoicesUrlTemplate"], invoiceNumber);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", _configuration["WebServiceInvoiceAuthKey"]);
                var response = await client.GetAsync(url);

                HttpResponseMessage result;
                if (response.IsSuccessStatusCode)
                {
                    result = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new ByteArrayContent(response.Content.ReadAsByteArrayAsync().Result)
                    };
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                    result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                    {
                        FileName = string.Format("{0}.pdf", invoiceNumber)
                    };
                }
                else
                {
                    result = new HttpResponseMessage(response.StatusCode);
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        private string GetInvoicePdfUrl(string invoiceNumber)
        {
            var url = string.Format(_configuration["InvoicesUrlTemplate"], invoiceNumber);
            var downloadUrl = string.Format(_configuration["WebServiceInvoicesUrlTemplate"], invoiceNumber);

            try
            {
                var request = WebRequest.Create(url);
                request.Method = "HEAD";
                request.Headers.Add(HttpRequestHeader.Authorization, string.Format("Basic {0}", _configuration["WebServiceInvoiceAuthKey"]));

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    return response.StatusCode == HttpStatusCode.OK ? downloadUrl : null;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return null;
            }

        }
    }
}
