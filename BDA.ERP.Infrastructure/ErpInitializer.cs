﻿using BDA.DataAccess;
using BDA.ERP.Data;
using BDA.ERP.Data.Contracts;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDA.ERP.Infrastructure
{
    public class ErpInitializer
    {
        private const string DefaultConnectionStringKey = "oracleConnection";
        public static void Initialize(IServiceCollection services, params ContextConfiguration[] configurations)
        {
            if (configurations.Length == 0)
                configurations = new[]
                {
                    new ContextConfiguration(ConfigurationManager.ConnectionStrings[DefaultConnectionStringKey]
                        .ConnectionString)
                };

            // data context factory
            services.AddScoped<IDataContextFactory<IErpRepository>>(ifact =>
                new DataContextFactory<ErpContext, IErpRepository>(configurations));

            // default context (unnamed) / transient, as it is mantained by the factory
            services.AddTransient(ifact => ifact.GetService<IDataContextFactory<IErpRepository>>().GetContext());

            // services

        }
    }
}
